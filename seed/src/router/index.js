import Vue from 'vue'
import Router from 'vue-router'

// layout components
import Full from '@/container/Full'

// dashboard components
import DashboardOne from '@/views/dashboard/DashboardOne'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Full,
      redirect: '/dashboard/dashboard-v1',
      children: [
        {
          path: '/dashboard/dashboard-v1',
          component: DashboardOne,
          meta: {
            title: 'Dashboard',
            breadcrumb: 'Dashboard / Dashboard'
          }
        }
      ]
    }
  ]
})
